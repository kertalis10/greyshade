﻿using GreyShade.Model;
using GreyShade.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreyShade.Presenter
{
    class Presenter
    {
        readonly IView view;
        readonly IModel model;

        public Presenter(IView _view, IModel _model)
        {
            view = _view;
            model = _model;
            view.OpenFile += OpenFile;
            view.SaveFile += SaveFile;
            view.ReduceImg += ReduceImg;
            view.ShowChoosenSet += Show;

            view.Run();
        }

        private void OpenFile(object sender, PathEventArgs e)
        {
            model.OpenFile(e.Path);
            view.DrawImages(model.ReturnBitmaps(), 0);
        }

        private void SaveFile(object sender, PathEventArgs e)
        {
            model.SaveFile(e.Path, e.Number);
        }

        private void ReduceImg(object sender, ReduceEventArgs e)
        {
            model.ReduceImg(e.TypeOfReduce, e.ValueOfReduce, e.SizeOfSquareTwoColor, e.ValueOfThreshholdTwoColor,  e.SizeOfSquareBackGround, e.ValueOfThresholdBackGround, e.ValueOfThresholdBackHgn, e.ValueOfHall, e.ValueOfHallPixels, e.ValueOfProcent);
            view.DrawImages(model.ReturnBitmaps(), model.GetCount());
        }

        private void Show(object sender, ShowEventArgs e)
        {
            model.Show(e.Number);
            view.DrawImages(model.ReturnBitmaps(), model.GetCount());
        }
    }
}
