﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreyShade.View
{
    interface IView
    {
        Bitmap[] BitmapsDisplayed { get; set; }
        void DrawImages(Bitmap[] bitmap, int count);
        void Run();

        event EventHandler<PathEventArgs> OpenFile;
        event EventHandler<PathEventArgs> SaveFile;
        event EventHandler<ReduceEventArgs> ReduceImg;
        event EventHandler<ShowEventArgs> ShowChoosenSet;
    }
}
