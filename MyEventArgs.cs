﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreyShade
{
    public class PathEventArgs : EventArgs
    {
        private readonly string path;
        private readonly int number;

        public PathEventArgs(string s, int n)
        {
            path = s;
            number = n;
        }

        public string Path
        {
            get { return this.path; }
        }

        public int Number
        {
            get { return this.number; }
        }
    }

    public class ReduceEventArgs : EventArgs
    {
        private readonly string typeOfReduce;
        private readonly int valueOfReduce;
        private readonly int sizeOfSquareTwoColor;
        private readonly int valueOfThreshholdTwoColor;
        private readonly int sizeOfSquareBackGround;
        private readonly int valueOfThresholdBackGround;
        private readonly int valueOfThresholdHgn;
        private readonly int valueOfHall;
        private readonly int valueOfHallPixels;
        private readonly double valueOfProcent;



        public ReduceEventArgs(string t, int vR, int sTC, int vTC, int sB, int vB, int vH, int hall, int hallPix, double proc)
        {
            typeOfReduce = t;
            valueOfReduce = vR;
            sizeOfSquareTwoColor = sTC;
            valueOfThreshholdTwoColor = vTC;
            sizeOfSquareBackGround = sB;
            valueOfThresholdBackGround = vB;
            valueOfThresholdHgn = vH;
            valueOfHall = hall;
            valueOfHallPixels = hallPix;
            valueOfProcent = proc;
        }

        public string TypeOfReduce
        {
            get { return this.typeOfReduce; }
        }

        public int ValueOfReduce
        {
            get { return this.valueOfReduce; }
        }

        public int SizeOfSquareTwoColor
        {
            get { return this.sizeOfSquareTwoColor; }
        }

        public int ValueOfThreshholdTwoColor
        {
            get { return this.valueOfThreshholdTwoColor; }
        }

        public int SizeOfSquareBackGround
        {
            get { return this.sizeOfSquareBackGround; }
        }

        public int ValueOfThresholdBackGround
        {
            get { return this.valueOfThresholdBackGround; }
        }

        public int ValueOfThresholdBackHgn
        {
            get { return this.valueOfThresholdHgn; }
        }

        public int ValueOfHall
        {
            get { return this.valueOfHall; }
        }

        public int ValueOfHallPixels
        {
            get { return this.valueOfHallPixels; }
        }

        public double ValueOfProcent
        {
            get { return this.valueOfProcent; }
        }
    }

    public class ShowEventArgs : EventArgs
    {
        private readonly int num;


        public ShowEventArgs(int n)
        {
            num = n;
        }

        public int Number
        {
            get { return this.num; }
        }
    }
}
