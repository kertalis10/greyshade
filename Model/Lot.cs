﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GreyShade.Model
{
    class Lot
    {
        public List<Point> List { get; private set; }
        public double Count { get; private set; }
        public double F0 { get; private set; }
        public double K1 { get; private set; }
        public double K2 { get; private set; }
        public double F1 { get; private set; }
        public double F2 { get; private set; }
        public double K3 { get; private set; }
        public double K4 { get; private set; }
        public double K5 { get; private set; }
        public double A { get; private set; }
        public double B { get; private set; }
        public double C { get; private set; }
        public double Real { get; private set; }

        public int MinX { get; private set; }
        public int MaxX { get; private set; }
        public int MinY { get; private set; }
        public int MaxY { get; private set; }




        public struct Point
        {
            public int height;
            public int width;
            public int intensivity;

            public Point(int h, int w, int i)
            {
                height = h;
                width = w;
                intensivity = i;
            }
        }

        public Lot(byte[,] arr, List<Table.Point> l, int side)
        {
            MaxX = MaxY = 0;
            MinX = arr.GetLength(0);
            MinY = arr.GetLength(1);

            int h, w;

            List = new List<Point>();

            foreach (var point in l)
            {
                h = (point.height - 1) * (side - 1); w = (point.width - 1) * (side - 1);

                for (int i = h + 1; i < h + side; i++)
                    for (int j = w + 1; j < w + side; j++)
                        if (arr[i, j] != 0)
                        {
                            List.Add(new Point(i, j, arr[i , j]));
                            arr[i, j] = 0;
                            if (i < MinX) MinX = i;
                            if (i > MaxX) MaxX = i;
                            if (j < MinY) MinY = j;
                            if (j > MaxY) MaxY = j;
                        }
            }

            Count = List.Count;

            F0 = K1 = K2 = 0;
            F1 = F2 = K3 = K4 = K5 = 0;

            foreach (var point in List)
            {
                F0 += point.intensivity;
                F1 += (point.intensivity * point.height);
                F2 += (point.intensivity * point.width);
                K1 += point.height;
                K2 += point.width;
                K3 += (point.width * point.height);
                K4 += (point.height * point.height);
                K5 += (point.width * point.width);
            }

            Calc();
        }

        void Calc()
        {
            double S1 = K1 * K2 - Count * K3;
            double S2 = F0 * K1 - Count * F1;
            double S3 = K1 * K1 - Count * K4;
            double S4 = F0 * K2 - Count * F2;
            double S7 = K2 * K2 - Count * K5;

            C = (S1 * S2 - S3 * S4) / (S1 * S1 - S7 * S3);
            B = (S2 - C * S1) / S3;
            A = (F0 - B * K1 - C * K2) / Count;

            int res;
            int temp = 0;

            foreach (var p in List)
            {
                res = (int)(A + B * p.height + C * p.width);

                if (Math.Abs(res - p.intensivity) < 10)
                    temp++;
            }

            Real = (100f / Count) * temp;
        }

        public double LotsCompare(Lot l, int hall)
        {
            int res = 0;
            int temp = 0;

            foreach (var p in List)
            {
                res = (int)(l.A + l.B * p.height + l.C * p.width);
                if (Math.Abs(res - p.intensivity) < hall)
                    temp++;
            }

            return (100f / Count) * temp;
        }

        public void Add(Lot l)
        {
            List.AddRange(l.List);
            Count += l.Count;
            F0 += l.F0;
            F1 += l.F1;
            F2 += l.F2;
            K1 += l.K1;
            K2 += l.K2;
            K3 += l.K3;
            K4 += l.K4;
            K5 += l.K5;
            if (l.MinX < MinX) MinX = l.MinX;
            if (l.MaxX > MaxX) MaxX = l.MaxX;
            if (l.MinY < MinY) MinY = l.MinY;
            if (l.MaxY > MaxY) MaxY = l.MaxY;

            Calc();
        }

        public bool CheckPoint(int height, int width, int intensivity, int hall)
        {
            if (intensivity != 255)
            {

                int res = (int)(A + B * height + C * width);

                if (Math.Abs(res - intensivity) < hall)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public byte[,] ReturnLotImage(byte[,] arr)
        {
            byte[,] tempArr = new byte[arr.GetLength(0), arr.GetLength(1)];

            for (int i = 0; i < arr.GetLength(0); i++)
                for (int j = 0; j < arr.GetLength(1); j++)
                    tempArr[i, j] = arr[i, j];

            foreach (var l in List)
            {
                tempArr[l.height - 1, l.width - 1] = 255;
            }

            return tempArr;
        }

        public void AddLotImage(byte[,] arr)
        {
            foreach (var p in List)
            {
                arr[p.height - 1, p.width - 1] = 255;
            }
        }
    }
}
