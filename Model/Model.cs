﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GreyShade.Model
{
    class Model : IModel
    {
        //0 - базовое, 1 - серые тона, 2 - умеьшенное серое, 3 - уменьшенное два цвета, 4 - уменьшенное, разбитое на множества
        Bitmap[] bitmaps = new Bitmap[6];
        byte[][,] arrays = new byte[6][,];
        Table tbl;
        byte[][,] arrayOfTables;
        List<Table> tables;

        public void OpenFile(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
                bitmaps[0] = new Bitmap(fs);

            arrays[1] = BitmapToByteRgb(bitmaps[0]);           
        }

        public void SaveFile(string path, int number)
        {
            bitmaps[number].Save(path, ImageFormat.Bmp);
        }

        //передача изображений презентеру, потом представлению
        public Bitmap[] ReturnBitmaps()
        {
            bitmaps[1] = RgbToBitmap(arrays[1]);

            if(arrays[2] != null)
                bitmaps[2] = RgbToBitmap(arrays[2]);

            if (arrays[3] != null)
                bitmaps[3] = RgbToColorBitmap(arrays[3]);

            if (arrays[4] != null)
                bitmaps[4] = RgbToBitmap(arrays[4]);

            if (arrays[5] != null)
                bitmaps[5] = RgbToColorBitmap(arrays[5]);

            return bitmaps;
        }        


        //type - тип уменьшения, valueTw - кратность уменьшения, 
        public void ReduceImg(string type, int valueTw, int countTc, int valueTrhTc, int countB, int valueB, int valueHgn, int valueHall, int valuePixelHall, double valueProc )
        {   
            //Первый этап - уменьшение изображения
            for (int i = 1; i < valueTw; i *= 2)
            {
                if (i == 1)
                {
                    if (type == "Avrg")
                    {
                        arrays[2] = ReduceAvg(arrays[1]);
                    }
                    else
                        arrays[2] = ReduceMax(arrays[1]);
                }
                else
                {
                    if (type == "Avrg")
                    {
                        arrays[2] = ReduceAvg(arrays[2]);
                    }
                    else
                        arrays[2] = ReduceMax(arrays[2]);
                }
            }

            arrayOfTables = new byte[16][,];

            tables = new List<Table>();

            SeparateImg();

            CreateTables(countB, valueB, valueHgn, valueHall, valuePixelHall, valueProc);

          
            //var watch = System.Diagnostics.Stopwatch.StartNew();

            Parallel.ForEach<Table>(tables, DoWork);

          //  foreach (var tbl in tables)
            //  tbl.DoWork();

           // watch.Stop();

            //MessageBox.Show(watch.ElapsedTicks.ToString());
           

            arrays[3] = TablesToHGNImg();

            arrays[5] = TablesToBGImg();

        }

        private void SeparateImg()
        {
            int height = arrays[2].GetLength(0);
            int width = arrays[2].GetLength(1);
            int newHeight = (int)Math.Ceiling((double)height / 4);
            int newWidth;
            int count = 0;
            byte[,] tempArr;

            for (int i = 0; i < height; i += newHeight)
            {
                newWidth = (int)Math.Ceiling((double)width / 4);
                for (int j = 0; j < width; j += newWidth)
                {
                    if (height - i < newHeight) newHeight = height - i;
                    if (width - j < newWidth) newWidth = width - j;
                    tempArr = new byte[newHeight, newWidth];

                    for (int h = 0; h < tempArr.GetLength(0); h++)
                        for (int w = 0; w < tempArr.GetLength(1); w++)
                            tempArr[h, w] = arrays[2][h + i, w + j];

                    arrayOfTables[count] = tempArr;
                    count++;
                }
            }
        }

        private void CreateTables(int countB, int valueB, int valueHgn, int valueHall, int valuePixelHall, double valueProc)
        {
            foreach (var tbl in arrayOfTables)
                tables.Add(new Table(tbl, countB, valueB, valueHgn, valueHall, valuePixelHall, valueProc));
        }

        private void DoWork(Table tbl)
        {
            tbl.DoWork();
        }

        private byte[,] TablesToHGNImg()
        {
            byte[,] tempImg = new byte[arrays[2].GetLength(0), arrays[2].GetLength(1)];


            int height = arrays[2].GetLength(0);
            int width = arrays[2].GetLength(1);
            int newHeight = (int)Math.Ceiling((double)height / 4);
            int newWidth = (int)Math.Ceiling((double)width / 4);
            int count = 0;


            for (int i = 0; i < height; i += tables[count - 1].arrayOfPixelHgen.GetLength(0))
                for (int j = 0; j < width; j += tables[count - 1].arrayOfPixelHgen.GetLength(1))
                {
                    for (int h = 0; h < tables[count].arrayOfPixelHgen.GetLength(0); h++)
                        for (int w = 0; w < tables[count].arrayOfPixelHgen.GetLength(1); w++)
                        {
                            tempImg[i + h, j + w] = tables[count].arrayOfPixelHgen[h, w];
                        }
                    count++;
                }

            return tempImg;
        }

        private byte[,] TablesToBGImg()
        {
            byte[,] tempImg = new byte[arrays[2].GetLength(0), arrays[2].GetLength(1)];


            int height = arrays[2].GetLength(0);
            int width = arrays[2].GetLength(1);
            int newHeight = (int)Math.Ceiling((double)height / 4);
            int newWidth = (int)Math.Ceiling((double)width / 4);
            int count = 0;


            for (int i = 0; i < height; i += tables[count - 1].arrayOfPixelBG.GetLength(0))
                for (int j = 0; j < width; j += tables[count - 1].arrayOfPixelBG.GetLength(1))
                {
                    for (int h = 0; h < tables[count].arrayOfPixelBG.GetLength(0); h++)
                        for (int w = 0; w < tables[count].arrayOfPixelBG.GetLength(1); w++)
                        {
                            tempImg[i + h, j + w] = tables[count].arrayOfPixelBG[h, w];
                        }
                    count++;
                }

            return tempImg;
        }



        private byte[,] ReduceMax(byte[,] arr)
        {
            byte[,] res = new byte[arr.GetLength(0) / 2, arr.GetLength(1) / 2];


            for (int i = 0; i < res.GetLength(0); i++)
                for (int j = 0; j < res.GetLength(1); j++)
                {                   
                    res[ i, j] = ReturnMax(arr[ i * 2, j * 2], arr[i * 2, j * 2 + 1], arr[i * 2 + 1, j * 2], arr[i * 2 + 1, j * 2 + 1]);
                   
                }

            return res;
        }

        private byte ReturnMax(byte b1, byte b2, byte b3, byte b4)
        {
            byte[] arr = new byte[] { b1, b2, b3, b4};

            Array.Sort(arr);

            return arr[0];           
        }

        private byte[,] ReduceAvg(byte[,] arr)
        {
            byte[,] res = new byte[arr.GetLength(0) / 2, arr.GetLength(1) / 2];


            for (int i = 0; i < res.GetLength(0); i++)
                for (int j = 0; j < res.GetLength(1); j++)
                {
                    res[i, j] = ReturnAvg(arr[i * 2, j * 2], arr[i * 2, j * 2 + 1], arr[i * 2 + 1, j * 2], arr[i * 2 + 1, j * 2 + 1]);
                }

            return res;
        }

        private byte ReturnAvg(byte b1, byte b2, byte b3, byte b4)
        {
            byte[] arr = new byte[] { b1, b2, b3, b4 };

            Array.Sort(arr);

            return (byte)((b1 + b2 + b3 + b4)/4);           
        }          

        private byte[,] BreakImage(byte[,] arr, int size, int value)
        {
            byte[,] res = new byte[arr.GetLength(0), arr.GetLength(1)];

            for (int i = 0; i < res.GetLength(0); i+=size)
            {               
                for (int j = 0; j < res.GetLength(1); j+=size)
                {
                     BreakPartOfImage(arr, res, i, j, size, value);        
                }               
            }

            return res;
        }

        private void BreakPartOfImage(byte[,] arr, byte[,] res, int x, int y, int size, int value)
        {
            int sizeX, sizeY;

            if (x + size <= arr.GetLength(0)) sizeX = size;
            else sizeX = arr.GetLength(0) - x;

            if (y + size <= arr.GetLength(1)) sizeY = size;
            else sizeY = arr.GetLength(1) - y;


            List<int> list = new List<int>(sizeX * sizeY);

            for (int i = x; i < x + sizeX; i++)
                for (int j = y; j < y + sizeY; j++)
                    list.Add(arr[i, j]);

            int temp = (int)(list.Average() - value);
            byte delta;

            if (temp < 0) delta = 0;           
            else delta = (byte)temp;

            for (int i = x; i < x + sizeX; i++)
                for (int j = y; j < y + sizeY; j++)
                {
                    if (arr[i, j] > delta) res[i, j] = 255;
                    else res[i, j] = 0;
                }
        }

        public void Show(int n)
        {
            bitmaps[4] = RgbToColorBitmap(tbl.ReturnSelectedLot(n, arrays[2]));            
        }

        public int GetCount()
        {
            // return tbl.GetCountofHgen();
            return 5;
        }

        //изображение в массив
        private unsafe byte[,] BitmapToByteRgb(Bitmap bmp)
        {
            int width = bmp.Width,
                height = bmp.Height;
            byte[,] res = new byte[height, width];
            BitmapData bd = bmp.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly,
                PixelFormat.Format24bppRgb);
            try
            {
                byte* curpos;
                for (int h = 0; h < height; h++)
                {
                    curpos = ((byte*)bd.Scan0) + h * bd.Stride;
                    for (int w = 0; w < width; w++)
                        res[h, w] = (byte)((*(curpos++) + *(curpos++) + *(curpos++)) / 3);
                }
            }
            finally
            {
                bmp.UnlockBits(bd);
            }
            return res;
        }

        //массив в изображение
        private unsafe static Bitmap RgbToBitmap(byte[,] rgb)
        {
            int width = rgb.GetLength(1),
                height = rgb.GetLength(0);

            Bitmap result = new Bitmap(width, height, PixelFormat.Format24bppRgb);

            BitmapData bd = result.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly,
                PixelFormat.Format24bppRgb);

            try
            {
                byte* curpos;
                for (int h = 0; h < height; h++)
                {
                    curpos = ((byte*)bd.Scan0) + h * bd.Stride;
                    for (int w = 0; w < width; w++)
                    {
                        *(curpos++) = rgb[h, w];
                        *(curpos++) = rgb[h, w];
                        *(curpos++) = rgb[h, w];
                    }
                }
            }
            finally
            {
                result.UnlockBits(bd);
            }

            return result;
        }

        private unsafe static Bitmap RgbToColorBitmap(byte[,] rgb)
        {
            int width = rgb.GetLength(1),
                height = rgb.GetLength(0);

            Bitmap result = new Bitmap(width, height, PixelFormat.Format24bppRgb);

            BitmapData bd = result.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly,
                PixelFormat.Format24bppRgb);

            try
            {
                byte* curpos;
                for (int h = 0; h < height; h++)
                {
                    curpos = ((byte*)bd.Scan0) + h * bd.Stride;
                    for (int w = 0; w < width; w++)
                    {
                        if (rgb[h, w] == 255)
                        {
                            *(curpos++) = 235;
                            *(curpos++) = 206;
                            *(curpos++) = 135;
                        }
                        else
                        {
                            *(curpos++) = rgb[h, w];
                            *(curpos++) = rgb[h, w];
                            *(curpos++) = rgb[h, w];
                        }
                    }
                }
            }
            finally
            {
                result.UnlockBits(bd);
            }

            return result;
        }

    }
}

