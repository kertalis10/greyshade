﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GreyShade.Model
{
    class Table
    {
        //Матрица базового изображения
        public byte[,] basicArray;
        //Матрица квадратов
        public byte[,] arrayOfHgen;
        //Матрица множеств-пикселей
        public byte[,] arrayOfPixelHgen;
        //матрица фона
        public byte[,] arrayOfPixelBG;
        //Набор множеств-квадратов
        List<List<Point>> listOfLotSuare;
        //Набор множеств-пикселей
        List<Lot> listOfLotPixel;
        

        int size;
        int delta;
        int valueHgn;
        int valueHall;
        int pixelHall;
        double valueProc;

        public struct Point
        {
            public int height;
            public int width;

            public Point(int h, int w)
            {
                height = h;
                width = w;
            }
        }

        public Table(byte[,] arr, int s, int d, int v, int hall, int pHall, double proc)
        {
            size = s;
            delta = d;
            valueHgn = v;
            valueHall = hall;
            valueProc = proc;
            basicArray = arr;
            pixelHall = pHall;

            arrayOfHgen = new byte[(int)Math.Ceiling(((double)basicArray.GetLength(0) / (size - 1))) + 2, (int)Math.Ceiling((double)basicArray.GetLength(1) / (size - 1)) + 2];
            
            listOfLotSuare = new List<List<Point>>();         
            
            listOfLotPixel = new List<Lot>(); 
        }

        public void DoWork()
        {
            FillArrayOfSquare();
            CombineSquares();
            FillListOfPixel();
            CombinePixels();
            ReturnPixelArray();
            ReturnFullBGArray();
        }

        //Заполнение матрицы квадратов
        void FillArrayOfSquare()
        {
            for (int i = 1; i < arrayOfHgen.GetLength(0) - 1; i++)
                for (int j = 1; j < arrayOfHgen.GetLength(1) - 1; j++)
                {
                    Hgen(basicArray, i - 1, j - 1, size);
                }
        }

        //Определение является ли квадрат однородным
        void Hgen(byte[,] arr, int h, int w, int s)
        {
            int height, width;
            byte value = 255;
            int step = s - 1;
            int sum = 0;
            int avg = 0;

            if (arr.GetLength(0) - h * step < s) height = arr.GetLength(0) - h * step; else height = s;
            if (arr.GetLength(1) - w * step < s) width = arr.GetLength(1) - w * step; else width = s;

            for (int i = h * step; i < h * step + height; i++)
                for (int j = w * step; j < w * step + width; j++)
                {
                    sum += arr[i, j];
                }

            avg = sum / (height * width);

            for (int i = h * step; i < h * step + height; i++)
                for (int j = w * step; j < w * step + width; j++)
                {
                    if (Math.Abs(arr[i, j] - avg) > delta)
                    {
                        value = 0;
                    }
                }

            arrayOfHgen[h + 1, w + 1] = value;
        }

        //Объединение множеств
        void CombineSquares()
        {            
            int tempCount = 0;
            List<Point> currentLot;

            for (int i = 1; i < arrayOfHgen.GetLength(0) - 1; i++)
                for (int j = 1; j < arrayOfHgen.GetLength(1) - 1; j++)
                {
                    if (arrayOfHgen[i, j] == 255)
                    {
                        currentLot = new List<Point>
                        {
                            new Point(i, j)
                        };
                        arrayOfHgen[i, j] = 254;
                        tempCount = 0;

                        while (tempCount < currentLot.Count)
                        {
                            int x = currentLot[tempCount].height;
                            int y = currentLot[tempCount].width;

                            if (arrayOfHgen[x - 1, y] == 255)
                            {
                                currentLot.Add(new Point(x - 1, y));
                                arrayOfHgen[x - 1, y] = 254;
                            }
                            if (arrayOfHgen[x + 1, y] == 255)
                            {
                                currentLot.Add(new Point(x + 1, y));
                                arrayOfHgen[x + 1, y] = 254;
                            }
                            if (arrayOfHgen[x, y - 1] == 255)
                            {
                                currentLot.Add(new Point(x, y - 1));
                                arrayOfHgen[x, y - 1] = 254;
                            }
                            if (arrayOfHgen[x, y + 1] == 255)
                            {
                                currentLot.Add(new Point(x, y + 1));
                                arrayOfHgen[x, y + 1] = 254;
                            }
                            tempCount++;                           
                        }

                        if (currentLot.Count >= valueHgn)
                            listOfLotSuare.Add(currentLot);
                    }
                }
        }

        //Создание коллеции множест-пикселей
        void FillListOfPixel()
        {
            byte[,] tempArr = new byte[basicArray.GetLength(0) + size, basicArray.GetLength(1) + size];

            for (int i = 0; i < basicArray.GetLength(0); i++)
                for (int j = 0; j < basicArray.GetLength(1); j++)
                    tempArr[i + 1, j + 1] = basicArray[i, j];

            foreach (var l in listOfLotSuare)
                listOfLotPixel.Add(new Lot(tempArr, l, size));

            listOfLotPixel = listOfLotPixel.OrderByDescending(x => x.Count).ToList();
        }


        //Объединение множеств-пикселей
        void CombinePixels()
        {
            int count1 = 0;
            int count2 = 1;

            while (count1 < listOfLotPixel.Count)
            {
                count2 = count1 + 1;
                while (count2 < listOfLotPixel.Count)
                {
                    double t = listOfLotPixel[count2].LotsCompare(listOfLotPixel[count1], valueHall);
                    if (t - valueProc >= 0)
                    {
                        listOfLotPixel[count1].Add(listOfLotPixel[count2]);
                        listOfLotPixel.RemoveAt(count2);
                    }
                    else
                        count2++;
                }
                count1++;
            }

            listOfLotPixel = listOfLotPixel.OrderByDescending(x => x.Count).ToList();
        }

        public byte[,] ReturnSelectedLot(int num, byte[,] arr)
        {
            return listOfLotPixel[num].ReturnLotImage(arr);
        }


       void ReturnFullBGArray()     
       {
            arrayOfPixelBG = new byte[basicArray.GetLength(0), basicArray.GetLength(1)];

            for (int i = 0; i < arrayOfPixelBG.GetLength(0); i++)
                for (int j = 0; j < arrayOfPixelBG.GetLength(1); j++)
                    arrayOfPixelBG[i, j] = arrayOfPixelHgen[i, j];

            foreach (var p in listOfLotPixel)
            {
                for (int i = p.MinX; i <= p.MaxX; i++)
                    for (int j = p.MinY; j <= p.MaxY; j++)
                        if (p.CheckPoint(i, j, arrayOfPixelBG[i - 1, j - 1], pixelHall))
                            arrayOfPixelBG[i - 1, j - 1] = 255;
            }           
        }

        void ReturnPixelArray()
        {
            arrayOfPixelHgen = new byte[basicArray.GetLength(0), basicArray.GetLength(1)];

            for (int i = 0; i < arrayOfPixelHgen.GetLength(0); i++)
                for (int j = 0; j < arrayOfPixelHgen.GetLength(1); j++)
                    arrayOfPixelHgen[i, j] = basicArray[i, j];

            foreach (var p in listOfLotPixel)
            {
                p.AddLotImage(arrayOfPixelHgen);
            }            
        }

        public int GetCountofHgen()
        {
            return listOfLotPixel.Count - 1;
        }
    }
}
