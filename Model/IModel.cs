﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreyShade.Model
{
    interface IModel
    {
        int GetCount();
        void OpenFile(string path);
        void SaveFile(string path, int number);
        void ReduceImg(string type, int valueReduce, int sizeTh, int valueTh, int sizeB, int valueB, int valueHgn, int valueHall, int valuePixelHall, double valueProc);
        void Show(int number);
        Bitmap[] ReturnBitmaps();
    }
}
