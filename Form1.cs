﻿using GreyShade.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GreyShade
{
    public partial class WinFormView : Form, IView
    {
        public Bitmap[] BitmapsDisplayed { get; set; }

        public event EventHandler<PathEventArgs> OpenFile;
        public event EventHandler<PathEventArgs> SaveFile;
        public event EventHandler<ReduceEventArgs> ReduceImg;
        public event EventHandler<ShowEventArgs> ShowChoosenSet;

        public WinFormView()
        {
            InitializeComponent();
        }

        public void Run()
        {
            this.WindowState = FormWindowState.Maximized;

            pictureBox.Size = new Size((this.Width - 100) / 3 * 4, this.Width - 100);

            scaleComboBox.SelectedIndex = 0;
            typeDecreaseComboBox.SelectedIndex = 0;
            twoColorSquareComboBox.SelectedIndex = 0;
            twoColorThreshComboBox.SelectedIndex = 0;
            backgroundThrasholdComboBox.SelectedIndex = 0;
            backgroundSquareSizeComboBox.SelectedIndex = 0;
            backgroundUnionComboBox.SelectedIndex = 0;
            hallComboBox.SelectedIndex = 0;
            procentComboBox.SelectedIndex = 0;

            Application.Run(this);
        }

        public void DrawImages(Bitmap[] bitmap, int count)
        {
            BitmapsDisplayed = bitmap;

            if(pictureBox.Image == null)
                pictureBox.Image = bitmap[0];

            if(bitmap[4] != null)
                pictureBox.Image = bitmap[4];

            uuionChoiceComboBox.Items.Clear();            

            for (int i = 0; i <= count; i++) 
                uuionChoiceComboBox.Items.Add(i);           
        }

        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {            
            RadioButton radioButton = (RadioButton)sender;
            if (radioButton.Checked)
            {
                ChangeImage(radioButton.Text);
            }
        }

        private void ChangeImage(string name)
        {
            if(name == "Basic")
                pictureBox.Image = BitmapsDisplayed[0];
            if (name == "Grey")
                pictureBox.Image = BitmapsDisplayed[1];
            if (name == "Reduce")
                pictureBox.Image = BitmapsDisplayed[2];
            if (name == "BackGround")
                pictureBox.Image = BitmapsDisplayed[3];
            if (name == "SelectedBG")
                pictureBox.Image = BitmapsDisplayed[4];
            if (name == "AllBG")
                pictureBox.Image = BitmapsDisplayed[5];
        }

        private void LoadButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog
            {
                Filter = "Image files(*.JPG; *.BMP)|*.JPG; *.BMP"
            };

            if (ofd.ShowDialog() == DialogResult.OK)
            {               
                OpenFile?.Invoke(this, new PathEventArgs(ofd.FileName, 0));
            }
        }      

        private void SaveReducedImageButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog
            {
                Filter = "Image files(*.BMP)|*.BMP"
            };

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                SaveFile?.Invoke(this, new PathEventArgs(sfd.FileName, 2));
            }
        }

        private void SaveTwoColorImageButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog
            {
                Filter = "Image files(*.BMP)|*.BMP"
            };

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                SaveFile?.Invoke(this, new PathEventArgs(sfd.FileName, 3));
            }
        }

        private void SaveBackGroundImageButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog
            {
                Filter = "Image files(*.BMP)|*.BMP"
            };

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                SaveFile?.Invoke(this, new PathEventArgs(sfd.FileName, 5));
            }
        }

        private void ReduceButton_Click(object sender, EventArgs e)
        {
            ReduceImg?.Invoke(this, new ReduceEventArgs(typeDecreaseComboBox.Text, Convert.ToInt32(scaleComboBox.Text), Convert.ToInt32(twoColorSquareComboBox.Text), Convert.ToInt32(twoColorThreshComboBox.Text), Convert.ToInt32(backgroundSquareSizeComboBox.Text), Convert.ToInt32(backgroundThrasholdComboBox.Text), Convert.ToInt32(backgroundUnionComboBox.Text), Convert.ToInt32(hallComboBox.Text), Convert.ToInt32(hallPixelsComboBox.Text), Convert.ToDouble(procentComboBox.Text)));
            pictureBox.Image = BitmapsDisplayed[2];
        }

        private void ShowChoosenSet_Click(object sender, EventArgs e)
        {
            if(uuionChoiceComboBox.Text != "" && BitmapsDisplayed.Length > 1)
                ShowChoosenSet?.Invoke(this, new ShowEventArgs(Convert.ToInt32(uuionChoiceComboBox.Text)));
        }
    }
}
