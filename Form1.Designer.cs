﻿namespace GreyShade
{
    partial class WinFormView
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxImgChs = new System.Windows.Forms.GroupBox();
            this.allBGButton1 = new System.Windows.Forms.RadioButton();
            this.selectedBGRadioButton = new System.Windows.Forms.RadioButton();
            this.bGColorRadioButton = new System.Windows.Forms.RadioButton();
            this.reduceRadioButton = new System.Windows.Forms.RadioButton();
            this.greyRadioButton = new System.Windows.Forms.RadioButton();
            this.basicRadioButton = new System.Windows.Forms.RadioButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LoadButton = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveTwoColorImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveBackGroundImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.scaleComboBox = new System.Windows.Forms.ComboBox();
            this.typeDecreaseComboBox = new System.Windows.Forms.ComboBox();
            this.reduceButton = new System.Windows.Forms.Button();
            this.twoColorSquareComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.twoColorThreshComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.procentComboBox = new System.Windows.Forms.ComboBox();
            this.hallComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.backgroundUnionComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.backgroundThrasholdComboBox = new System.Windows.Forms.ComboBox();
            this.backgroundSquareSizeComboBox = new System.Windows.Forms.ComboBox();
            this.uuionChoiceComboBox = new System.Windows.Forms.ComboBox();
            this.showUnion = new System.Windows.Forms.Button();
            this.hallPixelsComboBox = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBoxImgChs.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxImgChs
            // 
            this.groupBoxImgChs.Controls.Add(this.allBGButton1);
            this.groupBoxImgChs.Controls.Add(this.selectedBGRadioButton);
            this.groupBoxImgChs.Controls.Add(this.bGColorRadioButton);
            this.groupBoxImgChs.Controls.Add(this.reduceRadioButton);
            this.groupBoxImgChs.Controls.Add(this.greyRadioButton);
            this.groupBoxImgChs.Controls.Add(this.basicRadioButton);
            this.groupBoxImgChs.Location = new System.Drawing.Point(12, 27);
            this.groupBoxImgChs.Name = "groupBoxImgChs";
            this.groupBoxImgChs.Size = new System.Drawing.Size(539, 44);
            this.groupBoxImgChs.TabIndex = 3;
            this.groupBoxImgChs.TabStop = false;
            this.groupBoxImgChs.Text = "Type of image";
            // 
            // allBGButton1
            // 
            this.allBGButton1.Location = new System.Drawing.Point(378, 19);
            this.allBGButton1.Name = "allBGButton1";
            this.allBGButton1.Size = new System.Drawing.Size(93, 17);
            this.allBGButton1.TabIndex = 5;
            this.allBGButton1.Text = "AllBG";
            this.allBGButton1.UseVisualStyleBackColor = true;
            this.allBGButton1.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
            // 
            // selectedBGRadioButton
            // 
            this.selectedBGRadioButton.Location = new System.Drawing.Point(279, 19);
            this.selectedBGRadioButton.Name = "selectedBGRadioButton";
            this.selectedBGRadioButton.Size = new System.Drawing.Size(93, 17);
            this.selectedBGRadioButton.TabIndex = 4;
            this.selectedBGRadioButton.Text = "SelectedBG";
            this.selectedBGRadioButton.UseVisualStyleBackColor = true;
            this.selectedBGRadioButton.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
            // 
            // bGColorRadioButton
            // 
            this.bGColorRadioButton.Location = new System.Drawing.Point(185, 19);
            this.bGColorRadioButton.Name = "bGColorRadioButton";
            this.bGColorRadioButton.Size = new System.Drawing.Size(88, 17);
            this.bGColorRadioButton.TabIndex = 3;
            this.bGColorRadioButton.Text = "BackGround";
            this.bGColorRadioButton.UseVisualStyleBackColor = true;
            this.bGColorRadioButton.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
            // 
            // reduceRadioButton
            // 
            this.reduceRadioButton.AutoSize = true;
            this.reduceRadioButton.Location = new System.Drawing.Point(116, 19);
            this.reduceRadioButton.Name = "reduceRadioButton";
            this.reduceRadioButton.Size = new System.Drawing.Size(63, 17);
            this.reduceRadioButton.TabIndex = 2;
            this.reduceRadioButton.Text = "Reduce";
            this.reduceRadioButton.UseVisualStyleBackColor = true;
            this.reduceRadioButton.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
            // 
            // greyRadioButton
            // 
            this.greyRadioButton.AutoSize = true;
            this.greyRadioButton.Location = new System.Drawing.Point(63, 19);
            this.greyRadioButton.Name = "greyRadioButton";
            this.greyRadioButton.Size = new System.Drawing.Size(47, 17);
            this.greyRadioButton.TabIndex = 1;
            this.greyRadioButton.Text = "Grey";
            this.greyRadioButton.UseVisualStyleBackColor = true;
            this.greyRadioButton.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
            // 
            // basicRadioButton
            // 
            this.basicRadioButton.AutoSize = true;
            this.basicRadioButton.Checked = true;
            this.basicRadioButton.Location = new System.Drawing.Point(6, 19);
            this.basicRadioButton.Name = "basicRadioButton";
            this.basicRadioButton.Size = new System.Drawing.Size(51, 17);
            this.basicRadioButton.TabIndex = 0;
            this.basicRadioButton.TabStop = true;
            this.basicRadioButton.Text = "Basic";
            this.basicRadioButton.UseVisualStyleBackColor = true;
            this.basicRadioButton.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1021, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LoadButton,
            this.saveToolStripMenuItem,
            this.saveTwoColorImageToolStripMenuItem,
            this.saveBackGroundImageToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // LoadButton
            // 
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size(177, 22);
            this.LoadButton.Text = "Load";
            this.LoadButton.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.saveToolStripMenuItem.Text = "SaveReducedImage";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.SaveReducedImageButton_Click);
            // 
            // saveTwoColorImageToolStripMenuItem
            // 
            this.saveTwoColorImageToolStripMenuItem.Name = "saveTwoColorImageToolStripMenuItem";
            this.saveTwoColorImageToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.saveTwoColorImageToolStripMenuItem.Text = "SaveBGImage";
            this.saveTwoColorImageToolStripMenuItem.Click += new System.EventHandler(this.SaveTwoColorImageButton_Click);
            // 
            // saveBackGroundImageToolStripMenuItem
            // 
            this.saveBackGroundImageToolStripMenuItem.Name = "saveBackGroundImageToolStripMenuItem";
            this.saveBackGroundImageToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.saveBackGroundImageToolStripMenuItem.Text = "SaveFullBGImage";
            this.saveBackGroundImageToolStripMenuItem.Click += new System.EventHandler(this.SaveBackGroundImageButton_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(12, 78);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(539, 492);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 5;
            this.pictureBox.TabStop = false;
            // 
            // scaleComboBox
            // 
            this.scaleComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.scaleComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.scaleComboBox.FormattingEnabled = true;
            this.scaleComboBox.Items.AddRange(new object[] {
            "2",
            "4",
            "8",
            "16",
            "32",
            "64",
            "128"});
            this.scaleComboBox.Location = new System.Drawing.Point(171, 50);
            this.scaleComboBox.Name = "scaleComboBox";
            this.scaleComboBox.Size = new System.Drawing.Size(121, 21);
            this.scaleComboBox.TabIndex = 6;
            // 
            // typeDecreaseComboBox
            // 
            this.typeDecreaseComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.typeDecreaseComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeDecreaseComboBox.FormattingEnabled = true;
            this.typeDecreaseComboBox.Items.AddRange(new object[] {
            "Avrg",
            "Grt"});
            this.typeDecreaseComboBox.Location = new System.Drawing.Point(6, 50);
            this.typeDecreaseComboBox.Name = "typeDecreaseComboBox";
            this.typeDecreaseComboBox.Size = new System.Drawing.Size(121, 21);
            this.typeDecreaseComboBox.TabIndex = 7;
            // 
            // reduceButton
            // 
            this.reduceButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.reduceButton.Location = new System.Drawing.Point(787, 582);
            this.reduceButton.Name = "reduceButton";
            this.reduceButton.Size = new System.Drawing.Size(121, 23);
            this.reduceButton.TabIndex = 9;
            this.reduceButton.Text = "Reduce";
            this.reduceButton.UseVisualStyleBackColor = true;
            this.reduceButton.Click += new System.EventHandler(this.ReduceButton_Click);
            // 
            // twoColorSquareComboBox
            // 
            this.twoColorSquareComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.twoColorSquareComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.twoColorSquareComboBox.Enabled = false;
            this.twoColorSquareComboBox.FormattingEnabled = true;
            this.twoColorSquareComboBox.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "20"});
            this.twoColorSquareComboBox.Location = new System.Drawing.Point(6, 48);
            this.twoColorSquareComboBox.Name = "twoColorSquareComboBox";
            this.twoColorSquareComboBox.Size = new System.Drawing.Size(121, 21);
            this.twoColorSquareComboBox.TabIndex = 10;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.typeDecreaseComboBox);
            this.groupBox1.Controls.Add(this.scaleComboBox);
            this.groupBox1.Location = new System.Drawing.Point(681, 46);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(298, 88);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Настройки уменьшения";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(171, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Линейный масштаб";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Тип";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.twoColorThreshComboBox);
            this.groupBox2.Controls.Add(this.twoColorSquareComboBox);
            this.groupBox2.Location = new System.Drawing.Point(687, 174);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(298, 88);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Настройки разбиения на два цвета";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Размер квадрата";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(165, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Порог";
            // 
            // twoColorThreshComboBox
            // 
            this.twoColorThreshComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.twoColorThreshComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "5",
            "10",
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50"});
            this.twoColorThreshComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.twoColorThreshComboBox.Enabled = false;
            this.twoColorThreshComboBox.FormattingEnabled = true;
            this.twoColorThreshComboBox.Items.AddRange(new object[] {
            "5",
            "10",
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50"});
            this.twoColorThreshComboBox.Location = new System.Drawing.Point(165, 48);
            this.twoColorThreshComboBox.Name = "twoColorThreshComboBox";
            this.twoColorThreshComboBox.Size = new System.Drawing.Size(121, 21);
            this.twoColorThreshComboBox.TabIndex = 13;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.hallPixelsComboBox);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.procentComboBox);
            this.groupBox3.Controls.Add(this.hallComboBox);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.backgroundUnionComboBox);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.backgroundThrasholdComboBox);
            this.groupBox3.Controls.Add(this.backgroundSquareSizeComboBox);
            this.groupBox3.Location = new System.Drawing.Point(693, 309);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(298, 236);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Настройки выделения фона";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 170);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Процент";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(165, 100);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Коридор";
            // 
            // procentComboBox
            // 
            this.procentComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.procentComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "99",
            "99.1",
            "99.2",
            "99.3",
            "99.4",
            "99.5",
            "99.6",
            "99.7",
            "99.8",
            "99.9",
            "100"});
            this.procentComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.procentComboBox.FormattingEnabled = true;
            this.procentComboBox.Items.AddRange(new object[] {
            "95",
            "96",
            "97",
            "98",
            "99",
            "99,1",
            "99,2",
            "99,3",
            "99,4",
            "99,5",
            "99,6",
            "99,7",
            "99,8",
            "99,9",
            "100"});
            this.procentComboBox.Location = new System.Drawing.Point(9, 195);
            this.procentComboBox.Name = "procentComboBox";
            this.procentComboBox.Size = new System.Drawing.Size(121, 21);
            this.procentComboBox.TabIndex = 19;
            // 
            // hallComboBox
            // 
            this.hallComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.hallComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20"});
            this.hallComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.hallComboBox.FormattingEnabled = true;
            this.hallComboBox.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25"});
            this.hallComboBox.Location = new System.Drawing.Point(165, 131);
            this.hallComboBox.Name = "hallComboBox";
            this.hallComboBox.Size = new System.Drawing.Size(121, 21);
            this.hallComboBox.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 100);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(162, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Порог объединения множеств";
            // 
            // backgroundUnionComboBox
            // 
            this.backgroundUnionComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.backgroundUnionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.backgroundUnionComboBox.FormattingEnabled = true;
            this.backgroundUnionComboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "20",
            "30",
            "40",
            "50",
            "60",
            "70",
            "80",
            "90",
            "100"});
            this.backgroundUnionComboBox.Location = new System.Drawing.Point(6, 131);
            this.backgroundUnionComboBox.Name = "backgroundUnionComboBox";
            this.backgroundUnionComboBox.Size = new System.Drawing.Size(121, 21);
            this.backgroundUnionComboBox.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Размер квадрата";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(165, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Порог отклонения фона";
            // 
            // backgroundThrasholdComboBox
            // 
            this.backgroundThrasholdComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.backgroundThrasholdComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "5",
            "10",
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50"});
            this.backgroundThrasholdComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.backgroundThrasholdComboBox.FormattingEnabled = true;
            this.backgroundThrasholdComboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.backgroundThrasholdComboBox.Location = new System.Drawing.Point(165, 48);
            this.backgroundThrasholdComboBox.Name = "backgroundThrasholdComboBox";
            this.backgroundThrasholdComboBox.Size = new System.Drawing.Size(121, 21);
            this.backgroundThrasholdComboBox.TabIndex = 13;
            // 
            // backgroundSquareSizeComboBox
            // 
            this.backgroundSquareSizeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.backgroundSquareSizeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.backgroundSquareSizeComboBox.FormattingEnabled = true;
            this.backgroundSquareSizeComboBox.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "20"});
            this.backgroundSquareSizeComboBox.Location = new System.Drawing.Point(6, 48);
            this.backgroundSquareSizeComboBox.Name = "backgroundSquareSizeComboBox";
            this.backgroundSquareSizeComboBox.Size = new System.Drawing.Size(121, 21);
            this.backgroundSquareSizeComboBox.TabIndex = 10;
            // 
            // uuionChoiceComboBox
            // 
            this.uuionChoiceComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uuionChoiceComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.uuionChoiceComboBox.FormattingEnabled = true;
            this.uuionChoiceComboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.uuionChoiceComboBox.Location = new System.Drawing.Point(702, 633);
            this.uuionChoiceComboBox.Name = "uuionChoiceComboBox";
            this.uuionChoiceComboBox.Size = new System.Drawing.Size(121, 21);
            this.uuionChoiceComboBox.TabIndex = 14;
            // 
            // showUnion
            // 
            this.showUnion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.showUnion.Enabled = false;
            this.showUnion.Location = new System.Drawing.Point(866, 631);
            this.showUnion.Name = "showUnion";
            this.showUnion.Size = new System.Drawing.Size(121, 23);
            this.showUnion.TabIndex = 15;
            this.showUnion.Text = "Show";
            this.showUnion.UseVisualStyleBackColor = true;
            this.showUnion.Click += new System.EventHandler(this.ShowChoosenSet_Click);
            // 
            // hallPixelsComboBox
            // 
            this.hallPixelsComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.hallPixelsComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20"});
            this.hallPixelsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.hallPixelsComboBox.FormattingEnabled = true;
            this.hallPixelsComboBox.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25"});
            this.hallPixelsComboBox.Location = new System.Drawing.Point(165, 195);
            this.hallPixelsComboBox.Name = "hallPixelsComboBox";
            this.hallPixelsComboBox.Size = new System.Drawing.Size(121, 21);
            this.hallPixelsComboBox.TabIndex = 22;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(170, 170);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(116, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Коридор доп пиксели";
            // 
            // WinFormView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1021, 699);
            this.Controls.Add(this.uuionChoiceComboBox);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.showUnion);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.groupBoxImgChs);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.reduceButton);
            this.Name = "WinFormView";
            this.Text = "Form1";
            this.groupBoxImgChs.ResumeLayout(false);
            this.groupBoxImgChs.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxImgChs;
        private System.Windows.Forms.RadioButton bGColorRadioButton;
        private System.Windows.Forms.RadioButton reduceRadioButton;
        private System.Windows.Forms.RadioButton greyRadioButton;
        private System.Windows.Forms.RadioButton basicRadioButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LoadButton;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.ComboBox scaleComboBox;
        private System.Windows.Forms.ComboBox typeDecreaseComboBox;
        private System.Windows.Forms.Button reduceButton;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveTwoColorImageToolStripMenuItem;
        private System.Windows.Forms.ComboBox twoColorSquareComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox twoColorThreshComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox backgroundThrasholdComboBox;
        private System.Windows.Forms.ComboBox backgroundSquareSizeComboBox;
        private System.Windows.Forms.RadioButton selectedBGRadioButton;
        private System.Windows.Forms.ToolStripMenuItem saveBackGroundImageToolStripMenuItem;
        private System.Windows.Forms.ComboBox uuionChoiceComboBox;
        private System.Windows.Forms.Button showUnion;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox backgroundUnionComboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox procentComboBox;
        private System.Windows.Forms.ComboBox hallComboBox;
        private System.Windows.Forms.RadioButton allBGButton1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox hallPixelsComboBox;
    }
}

